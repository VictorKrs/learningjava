package com.javalessons.primitives;

public class WorkWithPrimitives {
    public byte a;
    public short b;
    public int c;
    public long d;
    public float e;
    public double f;
    public char ch;
    public boolean bool;
    public String str;

    public WorkWithPrimitives(){
        a = 10;
        b = 1000;
        c = 1000000;
        d = 100000000000L;
        e = 3.14f;
        f = 30.113123;
        ch = 'h';
        bool = true;
        str = "";
        System.out.println("Constructor");
    }

    public void printStr (){
        str += a + " ";
        str += b + " ";
        str += c + " ";
        str += d + " ";
        str += e + " ";
        str += f + " ";
        str += ch + " ";
        str += bool;
        System.out.println(str);
    }
}
