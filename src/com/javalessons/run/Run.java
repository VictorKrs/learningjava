package com.javalessons.run;

import com.javalessons.hello_world.HelloWorld;
import com.javalessons.primitives.WorkWithPrimitives;

public class Run {
    public static void main (String[] argv){
//        HelloWorld.print();
        WorkWithPrimitives pr = new WorkWithPrimitives();
        pr.printStr();
    }
}
